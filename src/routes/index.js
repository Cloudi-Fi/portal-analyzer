
const { Router, static } = require('express');
const results = require('./results');
const jwtVerifier = require('express-jwt');
const config = require('./../lib/config');

const router = Router();
// router.use(function (req, res, next) {
//     console.log('Time:', Date.now());
//     next();
//     });

router.use(jwtVerifier({ secret: config.jwtSecret}).unless({path: ['/','/assets','/img/*']}));

router.use(config.api.prefix, results);
router.get('/', (req, res) => {
    res.sendFile('views/index.html', {root: __dirname });
});
let path = config.analyzer.rootPath

router.use('/img', static(config.analyzer.rootPath + config.analyzer.sharedPath + config.analyzer.screenshotPath));

module.exports = router;
