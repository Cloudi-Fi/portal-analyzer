const { Router } = require('express');
const { check, validationResult } = require('express-validator');
const { readdirSync, existsSync, readFileSync } = require('fs')
const config = require('../lib/config');

const getDirectories = source =>
  readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name);

const getFiles = source =>
  readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isFile())
    .map(dirent => dirent.name);

const router = Router();

router.get('/', async (req, res) => {
  try {

    let rows = [ 'template', 'location' ];

    res.status(200).json( rows );
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'something went wrong while reading test results folder', error: err.message });
  }
});

router.get('/:target/results', async (req, res) => {
  try {
    // retrieve all test result for the provided JWT Token
    const target = req.params.target;
    const path = getPath(req);

    let rows = [];

    if (!existsSync(path)) {

      res.status(404).json({ message: 'test result type ' + target + ' not found' });
    } else {

      rows = getDirectories(path);
      res.status(200).json( rows );
    }

  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'something went wrong while reading test results folder', error: err.message });
  }
});

router.get('/:target/results/:id', async (req, res) => {
  try {
    // retrieve one specific test results for the provided JWT Token
    const id = req.params.id;
    const target = req.params.target;

    const path = getPath(req, id);

    if (!existsSync(path)) {
      res.status(404).json({ message: target + ' test result with id ' + id + ' not found' });
    } else {
      const rows = getTestResults(path);
      res.status(200).json( rows );
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'something went wrong while looking at a special test result' });
  }
});

router.get('/:target/status', async (req, res) => {
  try {
    // retrieve all test result for the provided JWT Token
    const target = req.params.target;
    const path = getPath(req);

    let rows = [];

    if (!existsSync(path)) {

      res.status(404).json({ message: 'test result type ' + target + ' not found' });
    } else {

      rows = getDirectories(path);
      let result = [];
      rows.forEach( element => {
        result.push(getLatestResults(path, element));
      });
      res.status(200).json( result );
    }

  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'something went wrong while reading test results folder', error: err.message });
  }
});

router.get('/:target/status/:id', async (req, res) => {
  try {
    // retrieve one specific test results for the provided JWT Token
    const id = req.params.id;
    const target = req.params.target;

    const path = getPath(req, id);

    if (!existsSync(path)) {
      res.status(404).json({ message: target + ' test result with id ' + id + ' not found' });
    } else {
      const status = getLatestResults(path);
      res.status(200).json( status );
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'something went wrong while looking at a special test result' });
  }
});

function getPath(req, id = '') {
  const companyId = req.user.data.company.companyid;
  const target = req.params.target || '';
  const configAnalyzer = config.analyzer;
  const path = configAnalyzer.rootPath + configAnalyzer.sharedPath + configAnalyzer.storagePath + '/' + companyId + '/' + target + '/' + id ;

  return path;
}

function getLatestResults(path, element = '') {
  const filename = path + '/' + element + '/latest.json';
  let json = {};
  if (existsSync(filename)) {
    json = JSON.parse(readFileSync(filename));
    json.id = element;
  }

  return json;
}

function getTestResults(path) {
  const jsonFiles = getFiles(path).filter(filename => filename.match(/\d+\.json$/));

  let rows = [];
  jsonFiles.forEach(filename => {
    let json = JSON.parse(readFileSync(path + '/' + filename));
    json.name = filename;
    rows.push(json);
  });

  return rows;
}

module.exports = router;