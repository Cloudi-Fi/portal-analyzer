
const express = require('express');
var bodyParser = require('body-parser');
const app = express();

const amqp = require('amqplib/callback_api');
const consumer = require('./lib/consumer');
const browser = require('./lib/browser');
const config = require('./lib/config');
const router = require('./routes');

consumer(config.mq, amqp, browser);

app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', router);
app.use((err, req, res, next) => {
	if (err.name === 'UnauthorizedError') {
        res.status(500).send(err.message);
	}
});

app.listen(8080, () => console.log('Example app listening on port 8080!'))
