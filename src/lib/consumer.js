var localAMQP = null;

module.exports = ( config, amqp, work ) => {
  localAMQP = amqp;
  amqpConnect(config, work);
}

function amqpConnect(config, work) {
  localAMQP.connect(config.url + "?heartbeat=60", function(err, amqpConn) {

    amqpConn.createChannel(function(err, ch) {
      if (closeOnErr(err)) return;
      ch.on("error", function(err) {
        console.error("[AMQP] channel error", err.message);
      });
      ch.on("close", function() {
        console.log("[AMQP] channel closed");
        amqpConnect(config, work);
      });
      ch.assertExchange(config.exchange, 'x-delayed-message', {
        durable: true
      });
      ch.prefetch(10);
      ch.assertQueue("portal_validation_lookup", { durable: true }, function(err, q) {
        if (closeOnErr(err)) return;
        config.topics.forEach(function(key) {
          ch.bindQueue(q.queue, config.exchange, key);
        });
        ch.consume(q.queue, function (msg) {
          work(msg, function(ok) {
            try {
              if (ok) {
                ch.ack(msg);
              } else {
                ch.reject(msg, true);
              }
            } catch (e) {
              closeOnErr(e, amqpConn);
            }
          });
        }, { noAck: false });
        console.log("Worker is started");
      });
    });
  });
}
function closeOnErr(err, amqpConn) {
  if (!err) return false;
  console.error("[AMQP] error", err);
  if (amqpConn)
    amqpConn.close();
  return true;
}