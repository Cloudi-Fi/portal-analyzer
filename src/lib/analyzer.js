const responseAnalyzers = require('./response-analyzers');
const pageAnalyzers = require('./page-analyzers');
const { writeFile } = require('fs');
const config = require('./config');
const mkdirp = require('mkdirp');

var results = [];

module.exports.complete = (screenshot, infoTemplate) => {
  let results = [];
  responseAnalyzers.forEach(analyze => {
    results.push(
      {
        "name": analyze.getName(),
        "items": analyze.getAndClear()
      });
  });
  pageAnalyzers.forEach(analyze => {
    results.push(
      {
        "name": analyze.getName(),
        "items": analyze.getAndClear()
      });
  });

  saveResult(results, screenshot, infoTemplate);
}

module.exports.scanReponse = (response) => {
  responseAnalyzers.forEach(analyze => {
    analyze(response);
  });
}

async function scanPage(page) {
  await Promise.all(pageAnalyzers.map(async analyze => {
    return await analyze.scan(page);
  }));
}

module.exports.scanPage = scanPage;

function saveResult(results, screenshot, infoTemplate) {
  let now = Date.now();
  let jsonContent = JSON.stringify({"time": now.toString(), "screenshot":"/img/" + screenshot + ".png", "response-analyzers": results});
  
  let path = config.analyzer.rootPath + config.analyzer.sharedPath + config.analyzer.storagePath
    + '/' + infoTemplate.companyId + '/' + infoTemplate.target + '/' + infoTemplate.id;

  mkdirp.sync(path, (err) => console.log(err));

  let filename = now + '.json';
  writeFile(path + '/' + filename, jsonContent, 'utf8', (err) => {
    if (err) {
      console.log("An error occured while writing JSON Object to File.");
      return console.log(err);
    }
  });
  writeFile(path + '/latest.json', jsonContent, 'utf8', (err) => {
    if (err) {
      console.log("An error occured while writing JSON Object to latest.json File.");
      return console.log(err);
    }
  });
}

