var externalLinks = [];

async function scan(page) {
    externalLinks = await page.evaluate(externalRefs);
}

function externalRefs() {
    const iframes = Array.from(document.querySelectorAll('iframe'));
    let externalLinks = [];
    iframes.map(iframe => iframe.contentWindow).forEach(w => {
        const links = Array.from(w.document.querySelectorAll('a'));
        links.map(link => link.href).forEach(url => {
        let hasExternal = (url.match(/^https?:\/\/([^\/]*[0-9:]*)\/.*$/) || []);
        if (hasExternal.length !== 0 && !hasExternal[1].match(/^.*cloudi-fi\.(net|com)[0-9:]*$/)) {
            externalLinks.push({"url": hasExternal[0], "domain": hasExternal[1]});
        }
        });
    });

    return externalLinks;
}

function externalRefsDocument() {
    let externalLinks = [];
    const links = Array.from(document.querySelectorAll('a'));
    links.map(link => link.href).forEach(url => {
    let hasExternal = (url.match(/^https?:\/\/([^\/]*[0-9:]*)\/.*$/) || []);
        if (hasExternal.length !== 0 && !hasExternal[1].match(/^.*cloudi-fi\.(net|com)[0-9:]*$/)) {
            externalLinks.push({"url": hasExternal[0], "domain": hasExternal[1]});
        }
    });

    return externalLinks;
}

module.exports.getName = () => "external-refs";

module.exports.getAndClear = () => { 
    let results = externalLinks;
    externalLinks = [];

    return results;
}

module.exports.scan = scan;
