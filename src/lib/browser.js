const puppeteer = require('puppeteer');
const analyzer = require('./analyzer');
const config = require('./config');
const uuidv1 = require('uuid/v1');

module.exports = (msg, cb) => {
  var message = JSON.parse(msg.content.toString());
  var url = message.url;

  
  puppeteer.launch(config.puppeteer).then(async browser => {
    let inputs = message;
    const page = await browser.newPage();
    await page.setRequestInterception(false);
    page.on('response', analyzer.scanReponse);
    await page.setViewport({ width: 6000, height: 1500 });
    await page.goto(url);
    await page.waitForSelector('div.page > iframe');
    await analyzer.scanPage(page);
    let screenshot = uuidv1();

    await page.screenshot({ path: config.analyzer.rootPath +
      config.analyzer.sharedPath +
      config.analyzer.screenshotPath + '/' +
      screenshot + '.png', fullPage: true });

    var externalLinks = [];
    
    cb(true);
    await browser.close();
    analyzer.complete(screenshot, inputs);
  });
}
