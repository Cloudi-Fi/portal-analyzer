const dotenv = require('dotenv');

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.APP_ENV || 'development';

const envFound = dotenv.config();
if (!envFound) {
  // This error should crash whole process

  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

module.exports = {
  /**
   * Your favorite port
   */
  port: parseInt(process.env.PORT, 10),

  /**
   * Your secret sauce
   */
  jwtSecret: process.env.JWT_SECRET || 'secret',

  /**
   * Used by winston logger
   * 
   */
  logs: {
    level: process.env.LOG_LEVEL || 'silly',
  },
  mq: {
    url: process.env.RABBITMQ_URL || 'amqp://localhost',
    exchange:  process.env.RABBITMQ_EXCHANGE || 'testing_topic',
    topics: ( process.env.RABBITMQ_TOPICS || 'portal.validation' ).split(',')
  },
  puppeteer: {
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    headless: true
  },
  analyzer: {
    storagePath: process.env.APP_ANALYZER || '/analyzer',
    sharedPath: process.env.APP_STORAGE || '/shared',
    rootPath: process.env.APP_ROOT || '/tmp',
    screenshotPath: process.env.APP_ANALYZER_IMG || '/analyzer-img',
  },
  /**
   * API configs
   */
  api: {
    prefix: '/api',
  }
};
