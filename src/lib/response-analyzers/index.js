const fourOFour = require('./four-o-four');
const externalAssets = require('./external-assets');

module.exports.fourOFour = fourOFour;
module.exports.externalAssets = externalAssets;
module.exports = [fourOFour, externalAssets]