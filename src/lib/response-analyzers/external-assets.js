var externalAssets = [];
module.exports = (response) => {
//    let hasExternal = (response.url().match(/^https?:\/\/(.*\.(?!cloudi-fi)\.[^.]+[0-9:]*)\/.*$/) || []);
    let hasExternal = (response.url().match(/^https?:\/\/([^\/]*[0-9:]*)\/.*$/) || []);

    if (hasExternal.length !== 0 && !hasExternal[1].match(/^.*cloudi-fi\.net[0-9:]*$/)) {
        externalAssets.push({"url": hasExternal[0], "domain": hasExternal[1]});
    }
}

module.exports.getName = () => "external-assets";

module.exports.getAndClear = () => { 
    let results = externalAssets;
    externalAssets = [];

    return results;
}