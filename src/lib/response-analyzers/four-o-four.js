var itemsNotFound = [];
module.exports = (response) => {
    if (response.status() > 399) {
        let extract = (response.url().match(/^https?:\/\/.*\/([^/]*)\??/) || []);
        if (extract.length !== 0) {
            itemsNotFound.push({"name": extract[1], "url": extract[0], "code": response.status()});
        }
    }
}

module.exports.getName = () => "four-o-four";

module.exports.getAndClear = () => { 
    let results = itemsNotFound;
    itemsNotFound = [];

    return results;
}